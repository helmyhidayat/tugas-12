<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function table()
    {
        return view('tugas13.table');
    }

    public function datatable()
    {
        return view('tugas13.datatable');
    }
}
