<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\SigninController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', 'SigninController@first');
Route::get('/login', 'SigninController@login');
Route::post('/welcome', 'SigninController@welcome');

Route::get('/table', 'TableController@table');
Route::get('/datatable', 'TableController@datatable');

Route::resource('/cast', 'CastController');
