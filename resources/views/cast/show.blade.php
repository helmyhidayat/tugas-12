@extends('adminlte::page')

@section('title', 'Detail Cast')



@section('content')
    <br>
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Gagal!</strong> {{ $error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endforeach
    @endif
    @if ($success = Session::get('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Selamat!</strong> {{ $success }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <br>
    <div class="section">
        <div class="card">
            <div class="card-header">
                <h5>Detail Data Cast</h5>
            </div>
            <div class="card-body">

                <br>
                <br>
                <div>
                    <h6 class="text-bold">Nama</h6>
                    <p>{{ $cast->nama }}</p>
                    <h6 class="text-bold">Umur</h6>
                    <p>{{ $cast->umur }}</p>
                    <h6 class="text-bold">Biografi</h6>
                    <p>{{ $cast->bio }}</p>
                </div>
            </div>
            <div class="card-footer">
                <form action="{{ route('cast.destroy', $cast) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="btn-group">
                        <a class="btn btn-secondary" href="{{ route('cast.edit', $cast) }}">Edit</a>
                        <button type="submit" class="btn btn-danger">Hapus data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
