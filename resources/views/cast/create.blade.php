@extends('adminlte::page')

@section('title', 'Create Cast')



@section('content')
    <br>
    @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Gagal!</strong> {{ $error }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endforeach
    @endif
    @if ($success = Session::get('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Selamat!</strong> {{ $success }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <br>
    <div class="section">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mb-3">Input Data Cast</h5>
                <form action="{{ route('cast.store') }}" method="POST">
                    @csrf
                    <div class="input-group mb-3 mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Nama Cast</span>
                        </div>
                        <input type="text" class="form-control" aria-label="Sizing example input"
                            aria-describedby="inputGroup-sizing-default" name="nama" required value="{{ old('nama') }}">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">Umur Cast</span>
                        </div>
                        <input type="text" class="form-control" aria-label="Sizing example input"
                            aria-describedby="inputGroup-sizing-default" name="umur" required value="{{ old('umur') }}">
                    </div>
                    <div class="form-group mb-3">
                        <label for="exampleFormControlTextarea1">Biografi Cast</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="bio"
                            required>{{ old('bio') }}</textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@stop
