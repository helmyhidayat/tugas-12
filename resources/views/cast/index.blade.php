@extends('adminlte::page')

@section('title', 'Cast')

{{-- @section('content_header')
    <h1>Data Cast</h1>
@stop --}}

@section('content')
    <br>
    @if ($errors->any())
        <div class="row">
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Gagal!</strong> {{ $error }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endforeach
        </div>
    @endif
    @if ($success = Session::get('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Selamat!</strong> {{ $success }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <br>
    <div class="section">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Data Cast</h5>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Umur</th>
                            <th scope="col">Bio</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($casts as $cast)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $cast->nama }}</td>
                                <td>{{ $cast->umur }}</td>
                                <td>{{ $cast->bio }}</td>
                                <td>
                                    <form action="{{ route('cast.destroy', $cast) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <div class="btn-group">
                                            <a href="{{ route('cast.show', $cast) }}" class="btn btn-success">Detail</a>
                                            <a class="btn btn-secondary" href="{{ route('cast.edit', $cast) }}">Edit</a>
                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <a href="{{ route('cast.create') }}" class="btn btn-outline-success float-right">Buat Baru</a>
            </div>
        </div>
    </div>
@stop
