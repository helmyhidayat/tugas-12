@extends('adminlte::page')

@section('title', 'Create Post')

@section('content_header')
    <h1>Buat Post Baru</h1>
@stop

@section('content')
    <form action="{{ route('post.store') }}" method="POST">
        @csrf

        <div class="container col-md-8">
            <div class="form-group mb-3">
                <label for="title">Judul Post</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="tuliskan judul post" required>
            </div>
            <div class="form-group mb-3">
                <label for="body">Isi Post</label>
                <textarea class="form-control" name="body" id="body" rows="5" required></textarea>
              </div>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@stop
