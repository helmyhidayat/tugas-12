<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Akun Baru</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        @csrf
        <div>
            <label for="firstname">First name:</label>
            <input type="text" name="firstname" required>
        </div>
        <br>
        <div>
            <label for="lastname">Last name:</label>
            <input type="text" name="lastname" required>
        </div>
        <div>
            <h4>Gender</h4>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label><br>
        </div>
        <div>
            <h4>Nationality</h4>
            <select name="nationality" id="nationality">
                <option value="ID">Indonesia</option>
                <option value="US">United State</option>
                <option value="UK">United Kingdom</option>
                <option value="Other">Other</option>
            </select>
        </div>
        <div>
            <h4>Language Spoken</h4>
            <input type="checkbox" id="ID" name="ID" value="ID">
            <label for="ID">Bahasa Indonesia</label><br>
            <input type="checkbox" id="EN" name="EN" value="EN">
            <label for="EN">English</label><br>
            <input type="checkbox" id="other2" name="other2" value="other2">
            <label for="other2">Other</label><br>
        </div>
        <div>
            <label for="bio">
                <h4>Bio</h4>
            </label>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        </div>
        <button type="submit">Sign Up</button>
    </form>
</body>

</html>
