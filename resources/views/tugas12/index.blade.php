<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Media Online</title>
</head>

<body>
    <h4>Media Online</h4>
    <h6>Sosial Media Developer</h6>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h6>Benefit Join di Media Online</h6>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h6>Cara Bergabung ke Media Online</h6>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/login">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
</body>

</html>
